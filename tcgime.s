; GIME Test Card
; Copyright (c) 2023 Ciaran Anscomb

rom_vmode	equ $e033

		include "dragonhw.s"

		org $4000
		orcc #$50

		ldd #$fff8
		tfr a,dp
		setdp $ff
		stb reg_pia1_pdrb

		ldd #$343c
		sta reg_pia0_cra	; HS disabled
		sta reg_pia1_cra	; printer FIRQ disabled
		stb reg_pia1_crb	; CART FIRQ disabled
		lda #$37
		sta reg_pia0_crb	; FS enabled lo->hi

		; SAM MPU rate = slow
		sta reg_sam_r1c
		sta reg_sam_r0c

		; GIME native graphics, MMU enabled
		lda #$4c
		sta $ff90

		; GIME video mode
		ldb #50
		lda rom_vmode	; bit 3 SET if PAL
		bita #8		; PAL?
		beq >		; no?  skip increment
		addb #24
!		stb skip_count
		ora #$83	; graphics, 8 lines per row
		sta $ff98

		; GIME video resolution
		lda #$0a	; 192 lines, 32 bytes per row, 16 colours
		sta $ff99

		; GIME display offset = $70e00
		ldd #$e1c0
		std $ff9d

		clra
		tfr a,dp
		setdp 0

		; Draw the test card

		ldx #$0e00
		leas -3,s
		ldb #4
		stb 2,s

loop2		ldd #$0808
		std ,s

		ldd #$0000
loop0		std 32,x
		std 64,x
		std ,x++
		std 32,x
		std 64,x
		std ,x++
		addd #$1111
		dec ,s
		bne loop0
		leax 64,x

loop1		std 32,x
		std 64,x
		std ,x++
		std 32,x
		std 64,x
		std ,x++
		addd #$1111
		dec 1,s
		bne loop1
		leax 64,x

		dec 2,s
		bne loop2

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		clr palette_base
		bsr set_lo_palette

main_loop

		lda reg_pia0_pdrb	; clear any outstanding IRQ
		sync			; wait for IRQ

skip_count	equ *+1
		ldb #50
		bsr line_delay
		bsr set_hi_palette

		ldb #21
		bsr line_delay
		bsr set_lo_palette

		bsr line_delay
		bsr set_hi_palette

		bsr line_delay
		bsr set_lo_palette

		bsr line_delay
		bsr set_hi_palette

		bsr line_delay
		bsr set_lo_palette

		bsr line_delay
		bsr set_hi_palette

		bsr line_delay
		bsr set_lo_palette

		bra main_loop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Wait exactly B * 57 cycles (scanlines)

;		ldb #nlines		;  +2
;		bsr line_delay		;  +7 = 9
;		(OR: ldb #nlines, lbsr lline_delay)
line_delay	nop			;  +2 = 11
lline_delay	pshs a,b		;  +7 = 18
		clra			;  +2 = 20
		leax <<0,x		;  +5 = 25
		lbra 20F		;  +5 = 30

10		lda #1	;  +2
		mul	; +11 = 13
		inca	;  +2 = 15
		mul	; +11 = 26
		inca	;  +2 = 28
		mul	; +11 = 39
20		inca	;  +2 = 41	;  +2 = 32
		mul	; +11 = 52	; +11 = 43
		decb	;  +2 = 54	;  +2 = 45
		bne 10B	;  +3 = 57	;  +3 = 48

		puls a,b,pc		;  +9 = 57

; Set upper palette to next 8 values

set_hi_palette	pshs a,b,x	; +9
		ldx #$ffb8
		bra 10F

; Set lower palette to next 8 values

;		bsr set_*			;   +7
set_lo_palette	pshs a,b,x			;   +9 =  16
		nop				;   +2 =  18
		ldx #$ffb0			;   +3 =  21
palette_base	equ *+1
10		lda #$00			;   +2 =  23
		ldb #8				;   +2 =  25
!		sta ,x+		; +6
		inca		; +2 = 8
		decb		; +2 = 10
		bne <		; +3 = 13 * 8 =	; +104 = 129
		sta palette_base		;   +5 = 134
		exg a,a				;   +8 = 142
		exg a,a				;   +8 = 150
		exg a,a				;   +8 = 158
		nop				;   +2 = 160
		puls a,b,x,pc			;  +11 = 171 (3 * 57)
