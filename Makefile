# Testcard
# Copyright (c) 2023 Ciaran Anscomb

all: tcsg4.cas tcsg4.wav tcgime.cas tcgime.wav testcard.dsk
.PHONY: all

####

ASM6809 = asm6809
BIN2CAS = bin2cas.pl
XROAR = xroar -q -ui null -ao null -no-ratelimit

CLEAN = 
EXTRA_DIST =

####

%.dz: %
	dzip -c $< > $@

####

# SG4 test card

tcsg4.bin: tcsg4.scr.dz

tcsg4.bin: tcsg4.s
	$(ASM6809) -C -l $(@:.bin=.lis) -o $@ $<

CLEAN += tcsg4.scr.dz tcsg4.lis tcsg4.bin

# GIME test card

tcgime.bin: tcgime.s
	$(ASM6809) -C -l $(@:.bin=.lis) -o $@ $<

CLEAN += tcgime.lis tcgime.bin

####

# Build cassette images

tcsg4.cas tcsg4.wav: tcsg4.bin
	bin2cas.pl -r 44100 --cue --autorun --eof-data --fast \
		-n TCSG4 -o $@ \
		-C $<

CLEAN += tcsg4.cas tcsg4.wav

tcgime.cas tcgime.wav: tcgime.bin
	bin2cas.pl -r 44100 --cue --autorun --eof-data --fast \
		-n TCGIME -o $@ \
		-C $<

CLEAN += tcgime.cas tcgime.wav

####

# Non-autorun cassette file for Dragon copy to disk

tcsg4-noar.cas: tcsg4.bin
	bin2cas.pl -o $@ -C $<

CLEAN += tcsg4-noar.cas

####

# Boot block.  Same image is used for DragonDOS and RSDOS disks.

booty.bin: booty.s
	$(ASM6809) -B -l $(@:.bin=.lis) -o $@ $<

CLEAN += booty.lis booty.bin

####

# Build cross-platform disk image

testcard.dsk: hybrid.dsk
testcard.dsk: tcsg4.bin tcgime.bin
testcard.dsk: tcsg4-noar.cas copy-dragon.txt
testcard.dsk: booty.bin

testcard.dsk:
	cp hybrid.dsk $@
	decb copy -2b tcsg4.bin -2b $@,TCSG4.BIN
	decb copy -2b tcgime.bin -2b $@,TCGIME.BIN
	$(XROAR) -cart dragondos -cart-rom dplus49b -disk-write-back -load-fd0 $@ -load-tape tcsg4-noar.cas -load-text copy-dragon.txt -timeout-motoroff 12
	dd if=booty.bin of=$@ bs=256 seek=2 conv=notrunc
	dd if=booty.bin of=$@ bs=256 seek=$(shell expr 34 \* 18) conv=notrunc

CLEAN += testcard.dsk testcard.dsk.bak

####

clean:
	rm -f $(CLEAN)
.PHONY: clean
