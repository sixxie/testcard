; Testcard
; Copyright (c) 2023 Ciaran Anscomb

		include "dragonhw.s"

		org $4000
		orcc #$50

		ldd #$fff8
		tfr a,dp
		setdp $ff
		stb reg_pia1_pdrb

		ldd #$343c
		sta reg_pia0_cra	; HS disabled
		sta reg_pia1_cra	; printer FIRQ disabled
		stb reg_pia1_crb	; CART FIRQ disabled
		lda #$37
		sta reg_pia0_crb	; FS enabled lo->hi

		; SAM MPU rate = slow
		sta reg_sam_r1c
		sta reg_sam_r0c

		; SAM display offset = $0400
		sta reg_sam_f0c
		sta reg_sam_f1s
		sta reg_sam_f2c

		; SAM VDG mode = SG4
		sta reg_sam_v0c
		sta reg_sam_v1c
		sta reg_sam_v2c

		; Platform detection.  FS timing is different on:
		; - PAL Dragon
		; - PAL CoCo 1/2
		; - NTSC Dragon or CoCo 1/2
		; - NTSC CoCo 3
		; - PAL CoCo 3

		ldx #0
		lda reg_pia0_pdrb	; clear outstanding IRQ
		sync			; wait for FS
		lda reg_pia0_pdrb	; clear new IRQ
10		lda reg_pia0_crb	;  +5 - test for IRQ
		bmi 20F			;  +3 = 8
		mul			; +11 = 19
		mul			; +11 = 30
		mul			; +11 = 41
		exg a,a			;  +8 = 49
		leax 1,x		;  +5 = 54
		bra 10B			;  +3 = 57
20		ldb #146		; NTSC Dragon or CoCo 1/2
		cmpx #288		; PAL = 312, NTSC = 262
		blo 30F
		ldb #171		; PAL Dragon
30

		lda $a000		; [$A000] points to ROM1 in CoCos
		anda #$20
		beq is_dragon
		lda $80fd
		cmpa #'2
		beq is_coco3
		cmpx #288
		blo is_dragon
		decb			; PAL CoCo 1/2
		bra is_dragon
is_coco3	incb			; NTSC CoCo 3
		cmpx #288
		blo is_dragon
		addb #2			; PAL CoCo 3
is_dragon	stb sync_lines

		clra
		tfr a,dp
		setdp 0

		ldx #tcsg4
		ldu #$0400
duz_loop	ldd ,x++
		bpl duz_run	; run of 1-128 bytes
		tstb
		bpl duz_7_7
duz_14_8	lslb		; drop top bit of byte 2
		asra
		rorb		; asrd
		leay d,u
		ldb ,x+
		bra 10F		; copy 1-256 bytes (0 == 256)
duz_7_7		leay a,u	; copy 1-128 bytes
10		lda ,y+
		sta ,u+
		incb
		bvc 10B		; count UP until B == 128
		bra 80F
1		ldb ,x+
duz_run		stb ,u+
		inca
		bvc 1B		; count UP until B == 128
80		cmpx #tcsg4_end
		blo duz_loop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main_loop

		ldb reg_pia0_pdrb	; clear any outstanding IRQ
		sync			; wait for IRQ

sync_lines	equ *+1
		ldb #$00
		bsr line_delay

		clra
		sta reg_pia1_pdrb

		exg a,a
		exg a,a
		exg a,a
		leax ,x
		ldb #35
		bsr line_delay

		lda #$08
		sta reg_pia1_pdrb

		bra main_loop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Wait exactly B * 57 cycles (scanlines)

;		ldb #nlines		;  +2
;		bsr line_delay		;  +7 = 9
;		(OR: ldb #nlines, lbsr lline_delay)
line_delay	nop			;  +2 = 11
lline_delay	pshs a,b		;  +7 = 18
		clra			;  +2 = 20
		leax <<0,x		;  +5 = 25
		lbra 20F		;  +5 = 30

10		lda #1	;  +2
		mul	; +11 = 13
		inca	;  +2 = 15
		mul	; +11 = 26
		inca	;  +2 = 28
		mul	; +11 = 39
20		inca	;  +2 = 41	;  +2 = 32
		mul	; +11 = 52	; +11 = 43
		decb	;  +2 = 54	;  +2 = 45
		bne 10B	;  +3 = 57	;  +3 = 48

		puls a,b,pc		;  +9 = 57

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

tcsg4		includebin "tcsg4.scr.dz"
tcsg4_end
